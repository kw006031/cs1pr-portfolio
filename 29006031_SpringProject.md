Module Title: Programming in C/C++
Module Code: CS1PC20
Student Number: 29006031
Type of Assignment: Individual
Weighting of the Assignment: 15%


**Introduction**

Collectibles/Power-ups/Enemies/Score/Moving platforms/Multiple lives and checkpoints/Multiple Levels

Collectibles -> it gives the player satisfaction when they manage to collect all of them
Collectibles should be integrated as add-ons you'd get on a normal order, such as sauces and drinks.
Power-ups -> it gives extra variety to the gameplay, making it less repetitive.
Power-ups should also come from this world, such as aprons, bigger pizzas instead of the regular slices to collect, pizza trays and hot sauces.
apron- temporary invulnerability
big pizza - extra life
pizza tray - throw pizzas at enemies
hot sauce - speed
Enemies -> it provides the player with a challenge rather than just walking around.
For this game I was thinking that the enemies could be the other popular foods such as Chicken Wings, Pasta Bowls, Hmaburgers, French Fries and Hot Dogs.
Score -> quantifies how well the player played, allowing players to compete among each other.
Collectibles give score.
Defeating enemies gives score.
Score decreases over time
Moving platforms -> it provides the player with challenge rather than just walking around
Make them look like pizza boxes
Multiple lives and Checkpoints -> this allows more casual players to also have a good time, rather than having the level reset after every death
Multiple levels -> it gives extra variety to the gameplay, making it less repetitive
For a pizza game, the many different levels should take place in the most famous pizza chains in the world, such as Domino's, Pizza Hut, Papa John's, Sabroso, 
Chuck E Cheese and so on.
Programming Style:
Spaces everywhere, long, explicit variable and function names like MaxHealth and IncreaseScore(float Value) and comments that explain the role of different 
function parts. It makes it easier to read and debug.

**Design**

Controls: W/A/S/D
W-jump
A/D- move left and right
S+W- fall down
Space bar- attack when you have the right powerup
Gameplay: collect all the slices of pizza necesarry to pass each level
Choice of language: C++
 
![g](/uploads/8a2489af324f7107b924529d8954429f/g.PNG)


**Development**
1) Collectibles
They spawn at specific coordinates.
They have a texture.
They move up and down according to a sinusoidal function.
They have a certain score assigned to them.
They dissapear after colliding with the player.
They play a sound when collected.
They increase the score when they disappear.
Possible problems: They overlap with other objects during their vertical oscillation(can be fixed by reducing the radius of the oscillation). They might
disappear after overlapping with a non-player entity(can be fixed by adding a check to see whether the overlapping entity is a player or not)



2) Power-ups
They spawn at specific coordinates.
They have a texture.
They move up and down according to a sinusoidal function.
They have a certain score assigned to them.
They dissapear after colliding with the player.
They play a sound when collected.
They give the player a certain effect after disappearing.
Possible problems: They overlap with other objects during their vertical oscillation(can be fixed by reducing the radius of the oscillation). They might
disappear after overlapping with a non-player entity(can be fixed by adding a check to see whether the overlapping entity is a player or not)
a) Apron - Temporary Invulnerability(inspired by the shining star in the Mario games)
Toggles a Boolean in the Character's class that makes him invulnerable to damage.
Sets a timer to toggle the Boolean back off.
Possible problems: The timer gets interrupted(possibly by a level transition), which then makes the boolean stay true for the rest of the game.
b)Big pizza -  Extra life
Increases life counter in player's statistics
c)Pizza tray - ranged attack
Enables a boolean in the character's class that allows him to attack enemies by pressing a certain key
Attacks -  have a texture, have a movement speed, have a duration, dissapear on collision with a wall or an enemy, have a damage statistic
Possible problems: Attack overlapping doesn't get picked up by enemies. Attacks don't disspear after the timer expires. Attacks don't disappear 
after colliding with a wall or enemy. Attacks collide with enemies multiple times -  solution varies depending on how collision is handled by the engine.
d)Hot sauce - speed
Increases player's movement speed
Decreases it after a timer expires
Possible problems: The timer gets interrupted(possibly by a level transition), which then makes the speed stay increased for the rest of the game



3)Enemies(inspired by the mushrooms and flowers in the Mario games)
They have HP.  
They have damage. 
They have texture. 
They have a certain path they follow -> they oscillate between 2 coordinates.
They have a certain movement speed.
When they collide with an attack, they take the attack's damage.
When they collide with the player, they deal the player damage.
They play a certain sound on hit.
They disappear when their HP is less or equal to 0.
They play a certain sound on Death.
Possible problems: Their collision stays active after death, so they still deal damage and absorb attacks (the entity should be completely deleted, 
not just hidden after death, or at least deactivate all the collisions)



4)Score
Should be displayed on screen, in a corner except during level transitions. Can be increased by pickups and decreases at a fixed rate over time.



5)Moving platforms(inspired by the moving platforms in the Mario games)
Have the same collision presets as the ground. Oscillate between 2 positions.



6)Multiple lives and Checkpoints
The start of the level is considered the last checkpoint activated when a new level starts. Checkpoints get activated via collision with the player.
Number of lives should be displayed in the corner of the screen.
When HP decreases to 0, the lives should go down by one and the player should be teleported to the last checkpoint activated.
When lives reach 0, the Game Over screen should be displayed and all controls should be disabled from the player. 


7)Multiple levels
Each level should have preset textures for the background(in this case, each level takes place in one of the most famous pizza chains), collectibles, enemies and goals.
Maybe even a different background song.

**Conclusions**
I found this project hard to begin with but taking a closer look at the picture provided in the powerpoint, I thought of comparing this game to the popular Mario games.
Every step I took, I thought of the game I used to play as a child, what it was about, what was the goal, what the levels were like,
what were the challenges and then I started writing about the implementations I would give to Pete's Pizza Party.




/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static void tick(void);
static void touch(Entity *other);

void initPizza(char *line)
{
	Entity *e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 1;

	e->texture = loadTexture("gfx/pizza.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->flags = EF_WEIGHTLESS;
	e->tick = tick;
	e->touch = touch;

	stage.pizzaTotal++;
}

static void tick(void)
{
	self->value += 0.1;

	self->y += sin(self->value);
}

static void touch(Entity *other)
{
	if (self->health > 0 && other == player)
	{
		self->health = 0;

		stage.pizzaFound++;

		if (stage.pizzaFound == stage.pizzaTotal)
		{
			playSound(SND_PIZZA_DONE, CH_PIZZA);
			exit(1);
		}
		else
		{
			playSound(SND_PIZZA, CH_PIZZA);
		}
	}
}


